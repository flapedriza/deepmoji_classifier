#!/usr/bin/env sh
set -ue

SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
DOCKERFILE_PATH=$SCRIPTPATH/../docker/Dockerfile-plots
CONTEXT_PATH=$SCRIPTPATH/..
DOCKER_IMAGE="plots"

docker build -f "$DOCKERFILE_PATH" -t "$DOCKER_IMAGE" "$CONTEXT_PATH"
