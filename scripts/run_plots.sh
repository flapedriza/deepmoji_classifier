#!/usr/bin/env sh
set -ue

SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
PROJECT_ROOT=$SCRIPTPATH/..
DOCKER_IMAGE_NAME="plots"

docker run -it \
  -v "$PROJECT_ROOT"/src/common:/src/common \
  -v "$PROJECT_ROOT"/src/plots:/src/plots \
  -v "$PROJECT_ROOT"/results:/results \
  -v "$PROJECT_ROOT"/datasets:/datasets \
  "$DOCKER_IMAGE_NAME" bash