#!/usr/bin/env sh
set -ue

SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
PROJECT_ROOT=$SCRIPTPATH/..
DOCKER_IMAGE_NAME="deepmoji"

docker run -it \
  --gpus='"device=1"' \
  -v "$PROJECT_ROOT"/src/common:/src/common \
  -v "$PROJECT_ROOT"/src/classify:/src/classify \
  -v "$PROJECT_ROOT"/datasets:/datasets \
  -v "$PROJECT_ROOT"/DeepMoji:/deepmoji \
  -v "$PROJECT_ROOT"/results:/results \
  "$DOCKER_IMAGE_NAME" bash