import abc
import argparse
from datetime import datetime
from math import ceil

from classify.utils import style
from classify.utils.deepmoji_tagger import DeepmojiTagger


class BaseDatasetProcessor(object):
    __metaclass__ = abc.ABCMeta

    default_bulk_size = 1000

    def __init__(self):
        self._parse_args()
        getter = self._dataset_getter()
        self.rows = getter.get_clean_rows_generator()
        self.tagger = DeepmojiTagger(debug=self.debug)

    def run(self):
        if self.verbose:
            self.start = datetime.now()
            self.partial_start = datetime.now()

        if self.only is not None:
            num_preds = self._run_only()
        else:
            num_preds = self._run()

        if self.verbose:
            end = datetime.now()
            took = end - self.start
            print('Got {} predictions in {}'.format(num_preds, took))

    def _get_bulk_texts(self):
        bulk = []
        bulk_size = min(self.only or self.bulk_size, self.bulk_size)
        for i in range(bulk_size):
            try:
                row = next(self.rows)
                text = row[self._text_column]
                labels = self._get_labels(row)
                bulk.append((text, labels))
            except StopIteration:
                return bulk, True
        return bulk, False

    def _process_bulk(self):
        texts, finished = self._get_bulk_texts()
        predictions = self.tagger.get_predictions([t[0] for t in texts])
        if self.only is not None:
            self._print_bulk(texts, predictions, self.only)
        if self.show_first:
            self._print_bulk(texts, predictions, self.show_first)
        if self.verbose:
            result = '{}OK{}'.format(style.OKGREEN, style.ENDC)
            if len(texts) != len(predictions):
                result = '{}MISMATCH{}'.format(style.WARNING, style.ENDC)
            print('Number of inputs: {}, Number of predictions: {} {}'.format(
                len(texts),
                len(predictions),
                result
            )
            )
            self._print_time_stats(len(texts))
        return len(texts), finished

    @staticmethod
    def _print_bulk(texts, predictions, num):
        for _, t, p in zip(range(num), texts, predictions):
            text, labels = t
            text = '||{}|| with labels [{}]'.format(text, labels or 'None')
            print('For text {}, predictions are:'.format(text))
            for pr in p:
                print(pr)
            print('---------')

    def _print_time_stats(self, nrows):
        partial_end = datetime.now()
        took = partial_end - self.partial_start
        self.partial_start = partial_end
        print('Processed {} rows in {}'.format(nrows, took))

    def _run_only(self):
        float_bulks = float(self.only) / float(self.bulk_size)
        int_bulks = int(ceil(float_bulks))
        nbulks = max(1, int_bulks)
        if self.verbose:
            print('Number of bulks: {}'.format(nbulks))
        total_processed = 0
        for _ in range(nbulks):
            processed_rows, finished = self._process_bulk()
            total_processed += processed_rows
            self.only -= processed_rows
            if finished:
                break
        return total_processed

    def _run(self):
        total_processed = 0
        while True:
            num_processed, finished = self._process_bulk()
            total_processed += num_processed
            if finished:
                break
        return total_processed

    @property
    @abc.abstractmethod
    def _text_column(self):
        """
        This property should return the column in the CSV that contains the text to be analyzed
        """

    @property
    @abc.abstractmethod
    def _dataset_getter(self):
        """
        Dataset getter class to obtain the cleaned rows of the dataset
        """

    @staticmethod
    def _get_labels(row):
        return None

    def _parse_args(self, argv=None):
        parser = argparse.ArgumentParser()
        parser.add_argument(
            '--show-first',
            help='Show the first n results of each bulk',
            type=int,
            default=0
        )
        parser.add_argument(
            '--bulk-size',
            help='Number of elements of each bulk (defaults to {})'.format(self.default_bulk_size),
            type=int,
            default=self.default_bulk_size
        )
        parser.add_argument(
            '--only',
            help='Process only n rows of the dataset',
            type=int,
            default=None
        )
        parser.add_argument(
            '-q',
            '--quiet',
            help='Do not show time stats',
            action='store_true'
        )
        parser.add_argument(
            '-d',
            '--debug',
            help='Debug mode',
            action='store_true'
        )
        args = parser.parse_args(argv)
        if args.only:
            args.show_first = 0
        args.verbose = not args.quiet
        self.__dict__.update(args.__dict__)
