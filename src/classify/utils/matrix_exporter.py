import abc
import csv
from collections import namedtuple
from datetime import datetime

from classify.utils.deepmoji_tagger import DeepmojiTagger

Result = namedtuple('Result', ['id', 'text', 'labels'])


class BaseMatrixExporter(object):
    __metaclass__ = abc.ABCMeta

    bulk_size = 1000

    def __init__(self, filepath):
        getter = self._dataset_getter()
        self.rows = getter.get_clean_rows_generator()
        self.tagger = DeepmojiTagger()
        self.filepath = filepath

    def run(self, f=None):
        num_bulks = 1
        close = False
        if f is None:
            f = open(self.filepath, 'w')
            close = True
        while True:
            start = datetime.now()
            finished = self._process_bulk(f)
            end = datetime.now()
            print('Bulk {} processed in {}'.format(num_bulks, end - start))
            num_bulks += 1
            if finished:
                break
        if close:
            f.close()

    def _get_bulk_rows(self):
        bulk = []
        for _ in range(self.bulk_size):
            try:
                row = next(self.rows)
                ident = self._get_id(row)
                text = self._get_text(row)
                labels = self._get_labels(row)
                bulk.append(Result(ident, text, labels))
            except StopIteration:
                return bulk, True
        return bulk, False

    @staticmethod
    def _write_data(rows, predictions, dest_file):
        if len(rows) != len(predictions):
            raise Exception('Rows and predictions should have the same size')

        writer = csv.writer(dest_file)
        for r, p in zip(rows, predictions):
            row = [r.id]
            row.extend(p)
            if isinstance(r.labels, list):
                row.extend(r.labels)
            else:
                row.append(r.labels)
            writer.writerow(row)

    def _process_bulk(self, dest_file):
        rows, finished = self._get_bulk_rows()
        texts = [r.text for r in rows]
        predictions = self.tagger.get_matrix(texts)

        self._write_data(rows, predictions, dest_file)
        return finished

    @staticmethod
    def _get_labels(row):
        return None

    @staticmethod
    def _get_id(row):
        return None

    @staticmethod
    @abc.abstractmethod
    def _get_text(row):
        """
        Given a row, returns the text
        """

    @property
    @abc.abstractmethod
    def _dataset_getter(self):
        """
        Dataset getter class to obtain the cleaned rows of the dataset
        """
