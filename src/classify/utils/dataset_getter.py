import abc
import csv
import os


class BaseDatasetGetter(object):
    __metaclass__ = abc.ABCMeta

    csv_delimiter = ','
    csv_quoting = csv.QUOTE_MINIMAL
    csv_has_headers = True
    encoding = 'utf-8'

    @property
    @abc.abstractmethod
    def _relative_dataset_path(self):
        """
        This property should return the path of the dataset relative to the dataset directory
        of  the project
        """

    def _get_dataset_path(self):
        root_path = '/datasets'
        return os.path.join(root_path, self._relative_dataset_path)

    def _clean_row(self, row):
        """
        This method should perform any necessary cleaning on the row, by default returns it as it
        is
        """
        return row

    def _is_row_relevant(self, row):
        """
        This method is used to filter the rows in the dataset, if this method returns true, it will
        be added to the rows to process, otherwise the row will be skipped.
        """
        return True

    def get_clean_rows_generator(self):
        filepath = self._get_dataset_path()
        with open(filepath, 'r') as f:
            reader = csv.reader(f)
            if self.csv_has_headers:
                next(reader, None)
            for r in reader:
                if self._is_row_relevant(r):
                    clean_row = self._clean_row(r)
                    yield clean_row

    def get_clean_rows_list(self):
        ret = []
        for r in self.get_clean_rows_generator():
            ret.append(r)

        return ret
