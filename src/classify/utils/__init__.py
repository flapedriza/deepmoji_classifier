def cached_property(prop):
    def get(self):
        try:
            return self._property_cache[prop]
        except AttributeError:
            self._property_cache = {}
            x = self._property_cache[prop] = prop(self)
            return x
        except KeyError:
            x = self._property_cache[prop] = prop(self)
            return x

    return property(get)
