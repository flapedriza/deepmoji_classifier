import json
import string

import emoji
import numpy as np

from common.mapping import EMOJI_MAPPING
from deepmoji.global_variables import VOCAB_PATH, PRETRAINED_PATH
from deepmoji.model_def import deepmoji_emojis
from deepmoji.sentence_tokenizer import SentenceTokenizer


class EmojiResult(object):
    __slots__ = ('_emoji', '_confidence', '_property_cache')

    def __init__(self, emoji, confidence):
        self._emoji = emoji
        self._confidence = confidence

    @property
    def emoji(self):
        return EMOJI_MAPPING[self._emoji]

    @property
    def emoji_unicode(self):
        return emoji.emojize(self.emoji, use_aliases=True)

    @property
    def percent_confidence(self):
        confidence = round(self._confidence * 100, 2)
        return confidence

    def __str__(self):
        return u'{emoji} : with {confidence}% confidence'.format(
            emoji=self.emoji_unicode,
            confidence=self.percent_confidence
        ).encode('utf-8')


class DeepmojiTagger(object):
    MAX_LEN = 30
    ALLOWED_CHARS = set(string.printable)

    def __init__(self, vocabulary_path=VOCAB_PATH, model_path=PRETRAINED_PATH, debug=False):
        self.tokenizer = self._get_tokenizer(vocabulary_path)
        self.model = self._get_model(model_path)
        self.debug = debug

    def _clean_inputs(self, inp):
        if isinstance(inp, str):
            inp = [inp]
        inputs = []
        for i in inp:
            # noinspection PyBroadException
            try:
                i = filter(lambda x: x in self.ALLOWED_CHARS, i)
                # Deepmoji only accepts unicode as input
                i = unicode(i)
                inputs.append(i)
            except Exception:
                if self.debug:
                    print('Could not clean input, which was:')
                    print(i)
        return inputs

    def _process_inputs(self, inp):
        clean_inputs = self._clean_inputs(inp)
        if not clean_inputs:
            return []
        return self._tokenize_sentences(clean_inputs)

    def get_predictions(self, inp, num=5):
        """
        Given an input returns a list of the top num EmojiResult for that input
        """
        tokenized = self._process_inputs(inp)
        predictions = self._get_top_emoji(tokenized, num)
        return predictions

    def get_matrix(self, inp):
        """
        Given an input returns a matrix containing the emoji confidences for each element
        """
        tokenized = self._process_inputs(inp)
        predictions = self.model.predict(tokenized)
        return predictions

    @staticmethod
    def _load_vocabulary(path):
        with open(path, 'r') as f:
            return json.load(f)

    def _get_tokenizer(self, path):
        vocabulary = self._load_vocabulary(path)
        return SentenceTokenizer(vocabulary, self.MAX_LEN)

    def _get_model(self, path):
        model = deepmoji_emojis(self.MAX_LEN, path)
        return model

    def _tokenize_sentences(self, sentences):
        tokenized, _, _ = self.tokenizer.tokenize_sentences(sentences)
        return tokenized

    def _get_top_emoji(self, tokenized, num=5):
        predictions = self.model.predict(tokenized)
        ret = []
        for prob in predictions:
            top = self._top_elements(prob, num)
            probs = [prob[i] for i in top]
            res = []
            for emoj, confidence in zip(top, probs):
                res.append(EmojiResult(emoj, confidence))
            ret.append(res)
        return ret[0] if len(ret) == 1 else ret

    @staticmethod
    def _top_elements(array, k):
        ind = np.argpartition(array, -k)[-k:]
        return ind[np.argsort(array[ind])][::-1]
