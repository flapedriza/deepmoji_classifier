import json

import emoji
import numpy as np

from common.mapping import EMOJI_MAPPING
from deepmoji.global_variables import VOCAB_PATH, PRETRAINED_PATH
from deepmoji.model_def import deepmoji_emojis
from deepmoji.sentence_tokenizer import SentenceTokenizer


class InputAnalyzer(object):
    INPUT_MESSAGE = 'Insert your input [CLEAR to clear, ctrl+d to stop]:'
    MAX_LEN = 30

    def __init__(self, vocabulary_path=VOCAB_PATH, model_path=PRETRAINED_PATH):
        self.tokenizer = self.get_tokenizer(vocabulary_path)
        self.model = self.get_model(model_path)

    @staticmethod
    def top_elements(array, k):
        ind = np.argpartition(array, -k)[-k:]
        return ind[np.argsort(array[ind])][::-1]

    @staticmethod
    def load_vocabulary(path):
        with open(path, 'r') as f:
            return json.load(f)

    def get_tokenizer(self, path):
        vocabulary = self.load_vocabulary(path)
        return SentenceTokenizer(vocabulary, self.MAX_LEN)

    def get_model(self, path):
        model = deepmoji_emojis(self.MAX_LEN, path)
        return model

    @classmethod
    def get_input(cls):
        try:
            inp = raw_input(cls.INPUT_MESSAGE)
        except Exception:
            return None

        if inp == 'STOP':
            return None

        return unicode(inp)

    @classmethod
    def input_loop(cls):
        ipt = cls.get_input()
        while ipt:
            if ipt.lower() == 'clear':
                cls.clear()
            else:
                yield ipt
            ipt = cls.get_input()
        print('\n')

    def get_top_emoji(self, tokenized, num=5):
        predictions = self.model.predict(tokenized)
        prob = predictions[0]
        top = self.top_elements(prob, num)
        probs = [prob[i] for i in top]

        return zip(top, probs)

    @staticmethod
    def report_results(top_emoji):
        for k, v in top_emoji:
            confidence = round(v * 100, 2)
            message = '{emoji} : with {confidence}% confidence'.format(
                emoji=EMOJI_MAPPING[k],
                confidence=confidence
            )
            emojized = emoji.emojize(message, use_aliases=True)
            print(emojized.encode('utf-8'))

    @staticmethod
    def clear():
        print(chr(27) + '[2j')
        print('\033c')
        print('\x1bc')

    def run(self):
        self.clear()
        for inp in self.input_loop():
            tokenized, _, _ = self.tokenizer.tokenize_sentences([inp])
            top_emoji = self.get_top_emoji(tokenized)
            self.report_results(top_emoji)


if __name__ == '__main__':
    InputAnalyzer().run()
