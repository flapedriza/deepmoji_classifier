import json

from classify.utils.dataset_benchmarker import BaseDatasetProcessor
from classify.utils.dataset_getter import BaseDatasetGetter


class DatasetGetter(BaseDatasetGetter):
    def __init__(self):
        with open('/results/kaggle_wiki_suspicious_5.json') as f:
            self.id_list = json.load(f)

    @property
    def _relative_dataset_path(self):
        return 'kaggle_wikipedia/train.csv'

    def _is_row_relevant(self, row):
        # good_class_columns = [2, 3, 4, 5, 6]
        # return any([int(row[n]) for n in good_class_columns])
        return row[0] in self.id_list


class DatasetProcessor(BaseDatasetProcessor):

    @property
    def _text_column(self):
        return 1

    @property
    def _dataset_getter(self):
        return DatasetGetter

    @staticmethod
    def _get_labels(row):
        csv_labels = ["toxic", "severe_toxic", "obscene", "threat", "insult", "identity_hate"]
        labels = []
        for v, l in zip(row[2:], csv_labels):
            if int(v):
                labels.append(l)

        return ', '.join(labels)


if __name__ == '__main__':
    DatasetProcessor().run()
