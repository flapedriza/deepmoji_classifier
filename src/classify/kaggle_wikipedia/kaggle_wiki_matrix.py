from classify.utils.dataset_getter import BaseDatasetGetter
from classify.utils.matrix_exporter import BaseMatrixExporter


class DatasetGetter(BaseDatasetGetter):
    any_label = range(2, 8)

    @property
    def _relative_dataset_path(self):
        return 'kaggle_wikipedia/train.csv'

    def _is_row_relevant(self, row):
        good_class_columns = self.any_label
        return True or any([int(row[n]) for n in good_class_columns])


class MatrixExporter(BaseMatrixExporter):

    @staticmethod
    def _get_text(row):
        return row[1]

    @staticmethod
    def _get_id(row):
        return row[0]

    @staticmethod
    def _get_labels(row):
        return row[2:]

    @property
    def _dataset_getter(self):
        return DatasetGetter


if __name__ == '__main__':
    filepath = '/results/kaggle_wiki_train.csv'
    exporter = MatrixExporter(filepath)
    exporter.run()
