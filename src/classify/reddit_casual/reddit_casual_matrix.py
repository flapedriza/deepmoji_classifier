import json

from classify.utils.dataset_getter import BaseDatasetGetter
from classify.utils.matrix_exporter import BaseMatrixExporter
from deepmoji.filter_utils import separate_emojis_and_text


class DatasetGetter(BaseDatasetGetter):
    max_lines = 100000

    @property
    def _relative_dataset_path(self):
        return 'reddit_conversations/reddit_casual.json'

    def get_clean_rows_generator(self):
        filepath = self._get_dataset_path()
        with open(filepath, 'r') as f:
            data = json.load(f)
        num_lines = 0
        for lines in data:
            for line in lines['lines']:
                text = line['text']
                emojis, words = separate_emojis_and_text(text)
                if words.strip().strip(u'\ufe0f'):
                    yield text
                    num_lines += 1
                    if num_lines >= self.max_lines:
                        break


class MatrixExporter(BaseMatrixExporter):
    @staticmethod
    def _get_text(row):
        return row

    @property
    def _dataset_getter(self):
        return DatasetGetter


if __name__ == '__main__':
    filepath = '/results/reddit.csv'
    exporter = MatrixExporter(filepath)
    exporter.run()
