from classify.utils.dataset_getter import BaseDatasetGetter
from classify.utils.deepmoji_tagger import DeepmojiTagger
from classify.utils.matrix_exporter import BaseMatrixExporter


class DatasetGetter(BaseDatasetGetter):
    csv_has_headers = False

    def __init__(self, kind):
        self.kind = kind

    @property
    def _relative_dataset_path(self):
        return 'neural_chat/conversations_{}.csv'.format(self.kind)

    def _is_row_relevant(self, row):
        return bool(row[0])


class MatrixExporter(BaseMatrixExporter):
    def __init__(self, filepath, kind):
        getter = self._dataset_getter(kind)
        self.rows = getter.get_clean_rows_generator()
        self.tagger = DeepmojiTagger()
        self.filepath = filepath

    @staticmethod
    def _get_text(row):
        return row[0]

    @property
    def _dataset_getter(self):
        return DatasetGetter


if __name__ == '__main__':
    for kind in ['toxic', 'notoxic']:
        path = '/results/conversations_emoji_{}.csv'.format(kind)
        exporter = MatrixExporter(path, kind)
        exporter.run()
