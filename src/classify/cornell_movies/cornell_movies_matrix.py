import string

from classify.utils.dataset_getter import BaseDatasetGetter
from classify.utils.matrix_exporter import BaseMatrixExporter


class DatasetGetter(BaseDatasetGetter):
    max_lines = -1

    @property
    def _relative_dataset_path(self):
        return 'cornell_movies/movie_lines.txt'

    @staticmethod
    def _has_alpha(text):
        return any(map(lambda c: c.isalpha(), text))

    def _is_row_relevant(self, row):
        return self._has_alpha(row[-1]) and row[-1].strip()

    def get_clean_rows_generator(self):
        filepath = self._get_dataset_path()
        num_lines = 0
        with open(filepath, 'r') as f:
            for line in f:
                row = line.split('+++$+++')
                if self._is_row_relevant(row):
                    yield row
                    num_lines += 1
                    if self.max_lines != -1 and num_lines >= self.max_lines:
                        break


class MatrixExporter(BaseMatrixExporter):
    bulk_size = 1000

    @staticmethod
    def _get_text(row):
        text = row[-1].strip()
        return text

    @staticmethod
    def _get_id(row):
        return row[0].strip(string.ascii_letters)

    @property
    def _dataset_getter(self):
        return DatasetGetter


if __name__ == '__main__':
    filepath = '/results/movie.csv'
    exporter = MatrixExporter(filepath)
    exporter.run()
