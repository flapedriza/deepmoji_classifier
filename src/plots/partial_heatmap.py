import re
from collections import namedtuple
from decimal import Decimal

file_path = '../../results/heatmap-2880195.out'

line_regex = re.compile(
    r'\[CV\] \.\.\.\.\.\.\.\.\.\.\.\.\.\.\.\.\. C=([0-9\.]+), gamma=([0-9\.e-]+), score=([0-9\.]+), total=[0-9\.]+min')  # noqa

Result = namedtuple('Result', ['c', 'gamma', 'score'])

results = []

with open(file_path) as f:
    for line in f:
        match = line_regex.match(line)
        if match:
            r = Result(c=Decimal(match.group(1)), gamma=Decimal(match.group(2)),
                       score=Decimal(match.group(3)))  # noqa
            results.append(r)

for c, gamma, score in sorted(results, key=lambda x: (x.c, x.gamma), reverse=True):
    print(f'C={c}, gamma={gamma}, score={score}')

print(len(list([(x.c, x.gamma) for x in results])))
