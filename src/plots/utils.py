import math
import os
from typing import Tuple

import numpy as np
from emoji import emojize
from matplotlib import pyplot as plt
from matplotlib.font_manager import FontProperties

try:
    from classify.utils import cached_property
except:
    def cached_property(*args, **kwargs):
        return None
from common.mapping import EMOJI_LIST

font_properties = FontProperties(fname='./JoyPixels.ttf')

NUM_EMOJI = len(EMOJI_LIST)


class BalancedDatasetSplitter(object):
    def __init__(self, dataset: np.ndarray):
        self.dataset = dataset

    def train_test_split(self, test_size: float) -> Tuple[np.ndarray, np.ndarray]:
        np.random.shuffle(self.dataset)
        total_positives = self.num_positives
        total_negatives = self.num_negatives

        test_positives_num = math.ceil(total_positives * test_size)
        test_negatives_num = math.ceil(total_negatives * test_size)

        test_positives = self._get_positive_slice(test_positives_num)
        test_negatives = self._get_negative_slice(test_negatives_num)
        test_set = np.concatenate((test_negatives, test_positives))

        train_positives = self._get_positive_slice(total_positives, test_positives_num)
        train_negatives = self._get_negative_slice(total_negatives, test_negatives_num)
        train_set = np.concatenate((train_negatives, train_positives))

        return train_set, test_set

    @cached_property
    def dataset_positives(self):
        return self.dataset[self.dataset[:, -1] == 1]

    @cached_property
    def dataset_negatives(self):
        return self.dataset[self.dataset[:, -1] == 0]

    @cached_property
    def num_positives(self):
        return len(self.dataset_positives)

    @cached_property
    def num_negatives(self):
        return len(self.dataset_negatives)

    def _get_positive_slice(self, n: int, start_point: int = 0):
        return self.dataset_positives[start_point: n]

    def _get_negative_slice(self, n: int, start_point: int = 0):
        return self.dataset_negatives[start_point: n]


########################
#      Box plots       #
########################

def save_boxplot(results_path, label, names, values, y_label, x_label='Emoji'):
    name_indexes = range(len(names) + 1)
    names = [''] + names
    plt.figure(figsize=(15, 5))
    plt.boxplot(values, sym='')
    plt.xticks(name_indexes, labels=names, fontproperties=font_properties)
    plt.title('Distribution for {}'.format(label))
    plt.ylabel(y_label)
    plt.xlabel(x_label)
    dest_path = os.path.join(results_path, '{}.png'.format(label))
    if not os.path.exists(os.path.dirname(dest_path)):
        os.makedirs(os.path.dirname(dest_path))
    plt.savefig(dest_path)
    plt.close()


def top_whisker(ls):
    q3 = np.percentile(ls, 75)
    q1 = np.percentile(ls, 25)
    iqr = q3 - q1
    maxim = q3 + 1.5 * iqr
    ordered = np.sort(ls)
    return ordered[np.argmin(ordered < maxim) - 1]


def generate_boxplots(data, order_key, results_path):
    for label, matrix in data.items():
        vals = zip(range(NUM_EMOJI), np.array(matrix, dtype=float))
        vals = sorted(vals, key=lambda value: order_key(value[1]), reverse=True)
        names = [emojize(EMOJI_LIST[v[0]], use_aliases=True) for v in vals]
        values = [v[1] for v in vals]
        y_label = 'confidence'
        save_boxplot(results_path, label, names, values, y_label)


def generate_boxplot(label, matrix, order_key, results_path):
    vals = zip(range(NUM_EMOJI), np.array(matrix, dtype=float))
    vals = sorted(vals, key=lambda value: order_key(value[1]), reverse=True)
    names = [emojize(EMOJI_LIST[v[0]], use_aliases=True) for v in vals]
    values = [v[1] for v in vals]
    y_label = 'confidence'
    save_boxplot(results_path, label, names, values, y_label)
########################
#     Freq plots       #
########################

def base_tops(tops):
    def fun():
        base = {}
        for t in tops:
            base[t] = np.zeros(NUM_EMOJI, dtype='int')
        base['total'] = 0
        return base

    return fun()


def get_top_n_indices(arr, n):
    """
    Given a numpy array of numeric values, returns a numpy array containing the indices of the
    biggest n values.
    Parameters
    ----------
    arr A numpy array with numeric values
    n Number of top values to get

    Returns
    -------
    A numpy array containing the indices ot the top n values in arr
    """
    return np.argpartition(arr, -n)[-n:]


def get_top_arrays(tops, emoji_vect):
    """
    For each top, get an array filled with zeros but wit a one in the positions of the n biggest
    values in the emoji vector.
    Returns
    Parameters
    ----------
    tops List of numbers for which to get the top n values in the vector
    emoji_vect A numpy array containing the confidence of each emoji

    Returns
    -------
    A dictionary containing for each element in tops a numpy array with all zeros but the top n
    values
    """
    res = {}
    for t in tops:
        # Create an array with zeros
        base = np.zeros(NUM_EMOJI, dtype='int')
        top_indices = get_top_n_indices(emoji_vect, t)
        # Mark the n elements that have the biggest values in the emoji vector using their indices
        base[top_indices] = 1
        res[t] = base
    return res


def save_frequency_plot(results_path, label, top, names, values, y_label, x_label='Emoji'):
    name_indexes = range(len(names) + 1)
    plt.figure(figsize=(15, 5))
    plt.bar(range(NUM_EMOJI), values)
    plt.xticks(name_indexes, labels=names, fontproperties=font_properties)
    plt.title('Top {} occurrences for {}'.format(top, label))
    plt.ylabel(y_label)
    plt.xlabel(x_label)
    dest_path = os.path.join(results_path, '{}_top{}.png'.format(label, top))
    if not os.path.exists(os.path.dirname(dest_path)):
        os.makedirs(os.path.dirname(dest_path))
    plt.savefig(dest_path)
    plt.close()


def generate_frequency_plots(data, results_path):
    for label, result in data.items():
        for t, vec in result.items():
            vals = zip(range(NUM_EMOJI), vec)
            vals = sorted(vals, key=lambda v: v[1], reverse=True)
            names = [emojize(EMOJI_LIST[v[0]], use_aliases=True) for v in vals]
            vals = [val[1] for val in vals]
            y_label = 'Percentage of ocurrences in top {}'.format(t)
            save_frequency_plot(results_path, label, t, names, vals, y_label)
