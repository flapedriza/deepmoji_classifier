#!/bin/bash -l
#
#SBATCH -J heatmap
#SBATCH -o heatmap-%j.out
#SBATCH -e heatmap-%j.err
#
#SBATCH --mail-user francesc.lapedriza@est.fib.upc.edu
#SBATCH --mail-type=ALL
#
#SBATCH --mem=16G
#SBATCH -c 16
#SBATCH -p medium

export PYTHONUNBUFFERED=TRUE
export PYTHONPATH="/home/usuaris/lapedriza/deepmoji_classifier/src"

virtualenv --python=/usr/bin/python3 "$TMPDIR"/env

source "$TMPDIR"/env/bin/activate

pip install --upgrade pip

pip install -r ../../requirements_plots.txt

python heatmap.py