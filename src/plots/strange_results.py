import csv
import json

import numpy as np

from plots.utils import get_top_arrays

data_path = '/results/kaggle_wiki_train.csv'
result_base_name = '/results/kaggle_wiki_suspicious_{}.json'

id_index = 0
emoji_vector_slice = slice(1, 65)
label_slice = slice(65, 71)

strange_emoji = [0, 11, 30, 33]

strange_ids = {
    1: [],
    5: []
}

with open(data_path, 'r') as f:
    reader = csv.reader(f)
    for r in reader:
        labels = r[label_slice]
        labelsa = list(filter(lambda a: a == '1', labels))
        if labelsa:
            emoji_vect = np.array(r[emoji_vector_slice], dtype='float')
            top_arrays = get_top_arrays(strange_ids.keys(), emoji_vect)
            for i, v in top_arrays.items():
                if any([v[i] for i in strange_emoji]):
                    strange_ids[i].append(r[id_index])
for k, v in strange_ids.items():
    with open(result_base_name.format(k), 'w') as f:
        json.dump(v, f)
