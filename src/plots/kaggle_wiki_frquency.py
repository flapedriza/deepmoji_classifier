import csv
from collections import defaultdict

import numpy as np

from plots.utils import base_tops, generate_frequency_plots, get_top_arrays

results_path = '../../results/wiki_frequencies'
data_path = '../../results/kaggle_wiki_train.csv'

labels = [
    'Toxic',
    'Severe toxic',
    'Obscene',
    'Threat',
    'Insult',
    'Identity hate'
]

tops = [1, 5, 10]

emoji_vector_slice = slice(1, 65)
label_slice = slice(65, 71)


def has_label(label):
    def fun(row):
        try:
            label_index = labels.index(label)
        except ValueError:
            return False
        value = row[label_slice][label_index]

        return bool(int(value))

    return fun


def any_label(row):
    label_list = row[label_slice]
    return any([bool(int(lab)) for lab in label_list])


def no_labels(row):
    return not any_label(row)


def get_data():
    by_label = defaultdict(lambda: base_tops(tops))
    classification = {label: has_label(label) for label in labels}
    classification['Overall'] = lambda x: True
    classification['Any label'] = any_label
    classification['None'] = no_labels

    with open(data_path) as f:
        reader = csv.reader(f)
        for r in reader:
            emoji_vect = np.array(r[emoji_vector_slice], dtype='float')
            for lab, check in classification.items():
                if check(r):
                    this_label = by_label[lab]
                    this_label['total'] += 1
                    top_arrays = get_top_arrays(tops, emoji_vect)
                    for t, arr in this_label.items():
                        if type(t) == int:
                            np.add(arr, top_arrays[t], out=arr)
    print(by_label['Overall'])
    return by_label


def normalize_data(data, really_normalize=True):
    for result in data.values():
        total = result.pop('total')
        for top, value in result.items():
            assert total * top == np.sum(value)
            if really_normalize:
                result[top] = value / total * 100
    return data


if __name__ == '__main__':
    data = get_data()
    data = normalize_data(data)
    generate_frequency_plots(data, results_path)
