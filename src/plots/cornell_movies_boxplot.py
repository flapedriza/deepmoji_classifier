import csv
from collections import defaultdict

from plots.utils import generate_boxplots, top_whisker

results_path = '../../results/movie_distributions'

emoji_vector_slice = slice(1, 65)


def get_data():
    by_label = defaultdict(lambda: [[] for _ in range(64)])

    with open('../../results/movie.csv') as f:
        reader = csv.reader(f)
        for r in reader:
            vect = r[emoji_vector_slice]
            for i, v in enumerate(vect):
                by_label['Overall'][i].append(v)

    return by_label


def main():
    data = get_data()
    generate_boxplots(data, top_whisker, results_path)


if __name__ == '__main__':
    main()
