import csv

import numpy as np

csv_path = '/results/kaggle_wiki_train.csv'
matrix_path = '/results/mat.txt'


def get_python_matrix():
    ret = []
    with open(csv_path, 'r') as f:
        reader = csv.reader(f)
        for r in reader:
            matrow = np.empty(65, dtype='float')
            matrow[:64] = r[1:65]
            matrow[64] = any(int(c) for c in r[65:71])
            ret.append(matrow)

    return ret


def main():
    np.savetxt(matrix_path, np.array(get_python_matrix(), dtype='float'))


if __name__ == '__main__':
    main()
