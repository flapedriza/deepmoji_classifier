import os
import pickle

import numpy as np
from matplotlib import pyplot as plt
from sklearn.metrics import precision_recall_curve, confusion_matrix, ConfusionMatrixDisplay
from sklearn.model_selection import train_test_split

DATA_FILE = '../../results/mat.txt'
NUM_FEATURES = 64
CLASS_INDEX = 64
FEATURE_VECTORS = np.s_[:, :NUM_FEATURES]
CLASSES = np.s_[:, CLASS_INDEX]

MODELS_FOLDER = '../../results/models'
MODEL = 'RBF SVM best parameters'
RECALLS = np.around(np.arange(0.1, 1, 0.1), 3)
PRECISIONS = np.around(np.arange(0.2, 0.75, 0.05), 2)

CONFUSION_MATRIX_FOLDER = '../../results/confusion_matrix'


def get_missed_non_toxic(conf_matrix):
    total_non_toxic = conf_matrix[0, 0] + conf_matrix[0, 1]
    missed_non_toxic = conf_matrix[0, 1]
    return np.round((missed_non_toxic / total_non_toxic) * 100, 2)


def get_missed_toxic(conf_matrix):
    total_toxic = conf_matrix[1, 0] + conf_matrix[1, 1]
    missed_toxic = conf_matrix[1, 0]
    return np.round((missed_toxic / total_toxic) * 100, 2)


def find_nearest_index(arr: np.ndarray, value: np.float64):
    rounded_arr = np.around(arr, 2)
    flip_index = np.abs(rounded_arr - value).argmin()
    index = len(arr) - flip_index
    return np.abs(arr - value).argmin()  # index


data = np.loadtxt(DATA_FILE)
x = data[FEATURE_VECTORS]
y = data[CLASSES]

x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.3)
x_validation, x_test, y_validation, y_test = train_test_split(x_test, y_test, test_size=2 / 3)

print('Loaded and split data')

model_filename = f'{MODEL}.pickle'
model_filepath = os.path.join(MODELS_FOLDER, model_filename)

with open(model_filepath, 'rb') as f:
    model = pickle.load(f)

print('Loaded model')

scores = model.decision_function(x_test)

print('Scores computed')

precision, recall, thresholds = precision_recall_curve(y_test, scores)


def get_values(attribute_list, attribute_name, samples):
    missed_non_toxic = np.zeros(len(samples), dtype='float')
    missed_toxic = np.zeros(len(samples), dtype='float')

    model.thresholds = {}

    for i, r in enumerate(samples):
        nearest_index = find_nearest_index(attribute_list, r)
        threshold = thresholds[nearest_index]
        model.thresholds[r] = threshold
        nearest = np.round(attribute_list[nearest_index], 2)
        print(f'Generate matrix for {attribute_name}={r}, nearest is {nearest}')
        predicted_y_test = np.array(scores > threshold, dtype='float')

        cm = confusion_matrix(y_test, predicted_y_test)
        missed_non_toxic[i] = get_missed_non_toxic(cm)
        missed_toxic[i] = get_missed_toxic(cm)
        d = ConfusionMatrixDisplay(cm, display_labels=['Non toxic', 'Toxic'])
        d.plot()
        plot_filename = f'{MODEL} {attribute_name}={r}.png'
        plot_filepath = os.path.join(CONFUSION_MATRIX_FOLDER, plot_filename)
        plt.savefig(plot_filepath)
        plt.close()

    generate_plot(attribute_name, samples, missed_non_toxic, missed_toxic)

    # model_filename = f'{MODEL}_thresholds.pickle'
    # model_filepath = os.path.join(MODELS_FOLDER, model_filename)
    # with open(model_filepath, 'wb') as f:
    #     pickle.dump(model, f)


def generate_plot(x_label, x_values, missed_non_toxic, missed_toxic):
    x = np.arange(len(x_values))
    width = 0.35

    fig, ax = plt.subplots()
    fig.set_figwidth(width * 2.5 * len(x_values))
    rects1 = ax.bar(x - width / 2, missed_non_toxic, width, label='False positives')
    rects2 = ax.bar(x + width / 2, missed_toxic, width, label='False negatives')

    ax.set_ylabel('Percentage (lower is better)')
    ax.set_title(f'Toxic/non-toxic classification by {x_label} for {MODEL}')
    ax.set_xlabel(f'{x_label.capitalize()}')
    ax.set_xticks(x)
    ax.set_xticklabels(np.around(x_values, 3))
    ax.legend()

    def autolabel(rects):
        """Attach a text label above each bar in *rects*, displaying its height."""
        for rect in rects:
            height = rect.get_height()
            ax.annotate('{}%'.format(height),
                        xy=(rect.get_x() + rect.get_width() / 2, height),
                        xytext=(0, 3),  # 3 points vertical offset
                        textcoords="offset points",
                        ha='center', va='bottom')

    autolabel(rects1)
    autolabel(rects2)

    fig.tight_layout()

    plt.savefig(f'../../results/missed_percentage {MODEL}_{x_label}.png')
    plt.close(fig)


get_values(precision, 'precision', PRECISIONS)
get_values(recall, 'recall', RECALLS)
