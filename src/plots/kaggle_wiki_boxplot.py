import csv
from collections import defaultdict

from plots.utils import generate_boxplots, top_whisker

labels = [
    'Toxic',
    'Severe toxic',
    'Obscene',
    'Threat',
    'Insult',
    'Identity hate'
]

results_path = '../../results/wiki_distributions'
data_path = '../../results/kaggle_wiki_train.csv'

emoji_vector_slice = slice(1, 65)
label_slice = slice(65, 71)


def has_label(label):
    def fun(row):
        try:
            label_index = labels.index(label)
        except ValueError:
            return False
        value = row[label_slice][label_index]

        return bool(int(value))

    return fun


def any_label(row):
    label_list = row[label_slice]
    return any([bool(int(lab)) for lab in label_list])


def no_labels(row):
    return not any_label(row)


def get_data():
    by_label = defaultdict(lambda: [[] for _ in range(64)])

    classification = {label: has_label(label) for label in labels}
    classification['Overall'] = lambda x: True
    classification['Any label'] = any_label
    classification['None'] = no_labels

    with open(data_path) as f:
        reader = csv.reader(f)
        for r in reader:
            vect = r[emoji_vector_slice]
            for lab, check in classification.items():
                if check(r):
                    for i, v in enumerate(vect):
                        by_label[lab][i].append(v)
    return by_label


def main():
    data = get_data()
    generate_boxplots(data, top_whisker, results_path)


if __name__ == '__main__':
    main()
