import os
import pickle

import numpy as np

DATA_FILE = '../../results/mat_movies.txt'
FEATURE_VECTORS = np.s_[:, 1:65]

MODELS_FOLDER = '../../results/models'
MODEL = 'RBF SVM best parameters_thresholds'
RECALLS = np.array(('0.2', '0.4', '0.6', '0.8'), dtype='float')

CLASSIFIED_FOLDER = '../../results/classified/cornell_movies'

data = np.loadtxt(DATA_FILE)

x = data[FEATURE_VECTORS]

model_filename = f'{MODEL}.pickle'
model_filepath = os.path.join(MODELS_FOLDER, model_filename)

with open(model_filepath, 'rb') as f:
    model = pickle.load(f)

print('Loaded model')

scores = model.decision_function(x)
print('Got scores')

for r in RECALLS:
    threshold = model.thresholds[r]
    classes = np.zeros(len(scores))
    classes[scores > threshold] = 1
    print(f'For threshold {r}:')
    print(f'Num positive: {len(classes[classes == 1])}')
    print(f'Num negative: {len(classes[classes == 0])}')
    new_data = np.insert(data, len(data[0]), classes, axis=1)
    filename = f'data_{r}_classified.txt'
    filepath = os.path.join(CLASSIFIED_FOLDER, filename)
    np.savetxt(filepath, new_data)
