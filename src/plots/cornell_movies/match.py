import ast
import os

import numpy as np
from tqdm import tqdm

LINES_PATH = '../../datasets/cornell_movies/movie_lines.txt'
CONVERSATIONS_PATH = '../../datasets/cornell_movies/movie_conversations.txt'

CLASSIFIED_PATH = '../../results/classified/cornell_movies'

RECALLS = np.array(('0.2', '0.4', '0.6', '0.8'), dtype='float')

lines = {}
with open(LINES_PATH, 'r', encoding='Windows-1252') as f:
    for line in tqdm(f, total=304713):
        line_id, rest = line.split('+++$+++', 1)
        lines[line_id.strip()] = rest

conversations = {}
line_conversations = {}
with open(CONVERSATIONS_PATH, 'r', encoding='Windows-1252') as f:
    for line_id, line in tqdm(enumerate(f), total=83097):
        data, members = map(lambda x: x.strip(), line.rsplit('+++$+++', 1))
        members = set(ast.literal_eval(members))
        for m in members:
            line_conversations[m] = line_id
        conversations[line_id] = (data, members)

for r in tqdm(RECALLS):
    toxic_conversations = set()
    non_toxic_conversations = set()
    results_file = f'data_{r}_classified.txt'
    results_filepath = os.path.join(CLASSIFIED_PATH, results_file)
    data = np.loadtxt(results_filepath)
    for line in tqdm(data, total=304382):
        line_id = f'L{int(line[0])}'
        conversation_id = line_conversations[line_id]
        if line[-1] == 1:
            toxic_conversations.add(conversation_id)
        else:
            non_toxic_conversations.add(conversation_id)

    non_toxic_conversations -= toxic_conversations

    lines_path = os.path.join(CLASSIFIED_PATH, f'lines_{r}')
    lines_path_toxic = os.path.join(lines_path, 'toxic')
    lines_path_no_toxic = os.path.join(lines_path, 'notoxic')
    if not os.path.exists(lines_path_toxic):
        os.makedirs(lines_path_toxic)
    if not os.path.exists(lines_path_no_toxic):
        os.makedirs(lines_path_no_toxic)
    toxic_lines_filepath = os.path.join(lines_path_toxic, 'movie_lines.txt')
    no_toxic_lines_filepath = os.path.join(lines_path_no_toxic, 'movie_lines.txt')
    toxic_conversations_filepath = os.path.join(lines_path_toxic, 'movie_conversations.txt')
    no_toxic_conversations_filepath = os.path.join(lines_path_no_toxic, 'movie_conversations.txt')

    with open(toxic_lines_filepath, 'w', encoding='utf-8') as toxic_lines_file, open(
            toxic_conversations_filepath, 'w', encoding='utf-8') as toxic_conversations_file:
        for c in toxic_conversations:
            data, members = conversations[c]
            conversation_line = f'{data} +++$+++ {list(members)}\n'
            toxic_conversations_file.write(conversation_line)

            for line_id in members:
                line_line = f'{line_id} +++$+++ {lines[line_id]}'
                toxic_lines_file.write(line_line)

    with open(no_toxic_lines_filepath, 'w', encoding='utf-8') as no_toxic_lines_file, open(
            no_toxic_conversations_filepath, 'w', encoding='utf-8') as no_toxic_conversations_file:
        for c in non_toxic_conversations:
            data, members = conversations[c]
            conversation_line = f'{data} +++$+++ {list(members)}\n'
            no_toxic_conversations_file.write(conversation_line)

            for line_id in members:
                line_line = f'{line_id} +++$+++ {lines[line_id]}'
                no_toxic_lines_file.write(line_line)
