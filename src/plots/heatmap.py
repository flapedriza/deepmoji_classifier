import multiprocessing
import pickle

import matplotlib
from sklearn.metrics import average_precision_score, precision_recall_curve, PrecisionRecallDisplay, \
    plot_confusion_matrix

matplotlib.use('agg')
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import Normalize

from plots.utils import BalancedDatasetSplitter

from sklearn.svm import SVC
from sklearn.model_selection import StratifiedShuffleSplit
from sklearn.model_selection import GridSearchCV

NUM_FEATURES = 64
CLASS_INDEX = 64
NUM_JOBS = multiprocessing.cpu_count()
NUM_RANGE_STEPS = 13
IRIS = False

print(f'Will run {NUM_JOBS} jobs')


# Utility function to move the midpoint of a colormap to be around
# the values of interest.

class MidpointNormalize(Normalize):

    def __init__(self, vmin=None, vmax=None, midpoint=None, clip=False):
        self.midpoint = midpoint
        Normalize.__init__(self, vmin, vmax, clip)

    def __call__(self, value, clip=None):
        x, y = [self.vmin, self.midpoint, self.vmax], [0, 0.5, 1]
        return np.ma.masked_array(np.interp(value, x, y))


# #############################################################################
# Load and prepare data set
#
# dataset for grid search
data = np.loadtxt('../../results/mat.txt')
feature_vectors = np.s_[:, :NUM_FEATURES]
classes = np.s_[:, CLASS_INDEX]

train, test = BalancedDatasetSplitter(data).train_test_split(test_size=0.9)

x_train = train[feature_vectors]
y_train = train[classes]

x_test = test[feature_vectors]
y_test = test[feature_vectors]

print('Got dataset')

X = x_train
y = y_train

# iris = load_iris()
# X = iris.data
# y = iris.target
#
# y[y > 1] = 1
#
# x_test = X
# y_test = y

# Dataset for decision function visualization: we only keep the first two
# features in X and sub-sample the dataset to keep only 2 classes and
# make it a binary classification problem.

X_2d = X[:, :2]
y_2d = y

# It is usually a good idea to scale the data for SVM training.
# We are cheating a bit in this example in scaling all of the data,
# instead of fitting the transformation on the training set and
# just applying it on the test set.

# scaler = StandardScaler()
# X = scaler.fit_transform(X)
# X_2d = scaler.fit_transform(X_2d)

# #############################################################################
# Train classifiers
#
# For an initial search, a logarithmic grid with basis
# 10 is often helpful. Using a basis of 2, a finer
# tuning can be achieved but at a much higher cost.

C_range = np.logspace(-2, 10, NUM_RANGE_STEPS)
gamma_range = np.logspace(-9, 3, NUM_RANGE_STEPS)
param_grid = dict(gamma=gamma_range, C=C_range)
cv = StratifiedShuffleSplit(n_splits=1, test_size=0.2)
grid = GridSearchCV(SVC(), param_grid=param_grid, cv=cv, verbose=3, n_jobs=NUM_JOBS)
grid.fit(X, y)

print("The best parameters are %s with a score of %0.2f"
      % (grid.best_params_, grid.best_score_))

with open('../../results/grid.pickle', 'wb') as f:
    pickle.dump(grid, f)


# Now we need to fit a classifier for all parameters in the 2d version
# (we use a smaller set of parameters here because it takes a while to train)

# C_2d_range = [1e-2, 1, 1e2]
# gamma_2d_range = [1e-1, 1, 1e1]
# classifiers = []
# for C in C_2d_range:
#     for gamma in gamma_2d_range:
#         clf = SVC(C=C, gamma=gamma)
#         clf.fit(X_2d, y_2d)
#         classifiers.append((C, gamma, clf))

# #############################################################################
# Visualization
#
# draw visualization of parameter effects

# plt.figure(figsize=(8, 6))
# xx, yy = np.meshgrid(np.linspace(-3, 3, 200), np.linspace(-3, 3, 200))
# for (k, (C, gamma, clf)) in enumerate(classifiers):
#     # evaluate decision function in a grid
#     Z = clf.decision_function(np.c_[xx.ravel(), yy.ravel()])
#     Z = Z.reshape(xx.shape)
#
#     # visualize decision function for these parameters
#     plt.subplot(len(C_2d_range), len(gamma_2d_range), k + 1)
#     plt.title("gamma=10^%d, C=10^%d" % (np.log10(gamma), np.log10(C)),
#               size='medium')
#
#     # visualize parameter's effect on decision function
#     plt.pcolormesh(xx, yy, -Z, cmap=plt.cm.RdBu)
#     plt.scatter(X_2d[:, 0], X_2d[:, 1], c=y_2d, cmap=plt.cm.RdBu_r,
#                 edgecolors='k')
#     plt.xticks(())
#     plt.yticks(())
#     plt.axis('tight')


def generate_precision_recall_plot(model_name, scores, y_test):
    average_precision = average_precision_score(y_test, scores)
    precision, recall, thresholds = precision_recall_curve(y_test, scores)
    viz = PrecisionRecallDisplay(
        precision=precision, recall=recall,
        average_precision=average_precision, estimator_name=model_name
    )
    plot = viz.plot()
    return average_precision, plot


def plot_results():
    scores = grid.cv_results_['mean_test_score'].reshape(len(C_range),
                                                         len(gamma_range))
    # Draw heatmap of the validation accuracy as a function of gamma and C
    #
    # The score are encoded as colors with the hot colormap which varies from dark
    # red to bright yellow. As the most interesting scores are all located in the
    # 0.92 to 0.97 range we use a custom normalizer to set the mid-point to 0.92 so
    # as to make it easier to visualize the small variations of score values in the
    # interesting range while not brutally collapsing all the low score values to
    # the same color.

    plt.figure(figsize=(8, 6))
    plt.subplots_adjust(left=.2, right=0.95, bottom=0.15, top=0.95)
    plt.imshow(scores, interpolation='nearest', cmap=plt.cm.hot,
               norm=MidpointNormalize(vmin=0.2, midpoint=0.92))
    plt.xlabel('gamma')
    plt.ylabel('C')
    plt.colorbar()
    plt.xticks(np.arange(len(gamma_range)), gamma_range, rotation=45)
    plt.yticks(np.arange(len(C_range)), C_range)
    plt.title('Validation accuracy')
    filepath = '../../results/heatmap.png'
    if IRIS:
        filepath += '_iris'
    plt.savefig(f'{filepath}.png')

    model = grid.best_estimator_
    c = model.C
    gamma = model.gamma
    model_name = f'RBF_SVM_c_{c}_gamma_{gamma}'
    if IRIS:
        model_name += '_iris'
    scores = model.decision_function(x_test)
    average_precision, plot = generate_precision_recall_plot(model_name, scores, y_test)
    plot.ax_.set_title('2-class Precision-Recall curve: '
                       'AP={0:0.2f}, C={c}, gamma={gamma}'.format(average_precision, c=c,
                                                                  gamma=gamma))
    plt.savefig(f'../../results/precision-recall/{model_name}.png')

    plot_confusion_matrix(model, x_test, y_test)
    plt.savefig(f'../../results/confusion_matrix/{model_name}.png')


plot_results()
