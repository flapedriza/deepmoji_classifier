import csv
import os
import pickle

import numpy as np

from plots.utils import generate_boxplot


def process(kind):
    DATA_FILE = f'../../results/conversations_emoji_{kind}.csv'

    if kind == 'movie':
        DATA_FILE = f'../../results/movie.csv'

    MODELS_FOLDER = '../../results/models'
    MODEL = 'RBF SVM best parameters_thresholds'

    results_path = '../../results/neural_chat_distributions'

    with open(DATA_FILE) as f:
        reader = csv.reader(f)
        data = np.array([np.array(l[1:-1], dtype='float') for l in reader])

    model_filename = f'{MODEL}.pickle'
    model_filepath = os.path.join(MODELS_FOLDER, model_filename)

    with open(model_filepath, 'rb') as f:
        model = pickle.load(f)

    if kind != 'movie':
        scores = model.decision_function(data)

        toxic = len(scores[scores >= 0])
        notoxic = len(scores[scores < 0])

        toxic_perc = np.round(toxic / len(scores) * 100, 2)
        notoxic_perc = np.round(notoxic / len(scores) * 100, 2)

        print(
            f'For {kind}: mean score: {np.mean(scores)}, toxic: {toxic} ({toxic_perc}%), no toxic: {notoxic}({notoxic_perc}%)')

    generate_boxplot(kind, data, np.median, results_path)


if __name__ == '__main__':
    for i in ['toxic', 'notoxic', 'movie']:
        process(i)
