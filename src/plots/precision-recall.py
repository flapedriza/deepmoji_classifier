import functools
import multiprocessing
import pickle
import time
from datetime import timedelta

import matplotlib.pyplot as plt
import numpy as np
from sklearn.metrics import average_precision_score, precision_recall_curve, \
    PrecisionRecallDisplay, plot_confusion_matrix
from sklearn.model_selection import train_test_split
from sklearn.svm import SVC


def timeit(fun):
    @functools.wraps(fun)
    def wrp(*args, **kwargs):
        name = fun.__name__
        start = time.time()
        result = fun(*args, **kwargs)
        end = time.time()
        elapsed = timedelta(seconds=end - start)
        print(f'{name} for {args[0]} took {elapsed}')
        return result

    return wrp


NUM_FEATURES = 64
CLASS_INDEX = 64

NUM_JOBS = multiprocessing.cpu_count()

# classifiers = [
#     ('Isolation Forest', IsolationForest(), 'decision_function'),
#     ('Linear SVM Classifier', LinearSVC(), 'decision_function'),
#     ('Logistic Regression', LogisticRegression(), 'predict_proba'),
#     ('Random Forest Classifier', RandomForestClassifier(max_features=None), 'predict_proba'),
#     ('Naive Bayes', GaussianNB(), 'predict_proba'),
#     ('One Class SVM', OneClassSVM(), 'decision_function')
# ]
classifiers = [
    # ('Linear SVM', SVC(kernel='linear'), 'decision_function'),
    # ('Polynomial SVM', SVC(kernel='poly'), 'decision_function'),
    (
        'RBF SVM Default',
        SVC(kernel='rbf', verbose=True),
        'decision_function'
    ),
    (
        'RBF SVM best parameters',
        SVC(kernel='rbf', gamma=0.1, C=1000000.0, verbose=True),
        'decision_function'
    )
    # ('Sigmoid SVM', SVC(kernel='sigmoid'), 'decision_function'),
]


def load_and_split_data():
    data = np.loadtxt('../../results/mat.txt')

    print('Matrix loaded')

    feature_vectors = np.s_[:, :NUM_FEATURES]
    classes = np.s_[:, CLASS_INDEX]

    x_train, x_test, y_train, y_test = train_test_split(
        data[feature_vectors],
        data[classes],
        test_size=0.3
    )
    # x_train = preprocessing.scale(x_train)
    # x_test = preprocessing.scale(x_test)
    return x_train, x_test, y_train, y_test


@timeit
def train_model(model, x_train, y_train):
    model.fit(x_train, y_train)


@timeit
def get_scores(model, score_function_name, x_test):
    score_function = getattr(model, score_function_name)
    scores = score_function(x_test)
    if score_function_name == 'predict_proba':
        scores = np.max(scores, axis=1)
    return scores


@timeit
def generate_precision_recall_plot(model_name, scores, y_test):
    average_precision = average_precision_score(y_test, scores)
    precision, recall, thresholds = precision_recall_curve(y_test, scores)
    viz = PrecisionRecallDisplay(
        precision=precision, recall=recall,
        average_precision=average_precision, estimator_name=model_name
    )
    plot = viz.plot()
    return average_precision, plot


def make_plots(x_train, x_test, y_train, y_test):
    for model_name, model, score_function_name in classifiers:
        c = model.C
        gamma = np.round(1 / (64 * x_train.var()), 2)
        gamma_test = 1 / (64 * x_test.var())
        print(
            f'For {model_name} params are {model.get_params()}, c is {c} gamma is'
            f' {gamma} gamma test is {gamma_test}'
        )
        train_model(model, x_train, y_train)
        with open(f'../../results/models/{model_name}.pickle', 'wb') as f:
            pickle.dump(model, f)

        scores = get_scores(model, score_function_name, x_test)

        np.savetxt(f'../../results/scores/{model_name}_scores.txt', scores)
        np.savetxt(f'../../results/scores/{model_name}_dataset.txt', x_test)

        average_precision, plot = generate_precision_recall_plot(model_name, scores, y_test)
        try:
            gamma_label = str(float(model.gamma))
        except ValueError:
            gamma_label = gamma

        plot.ax_.set_title(
            '2-class Precision-Recall curve: AP={0:0.2f}, C={c}, gamma={gamma}'.format(
                average_precision, c=c, gamma=gamma_label
            )
        )
        plt.savefig(f'../../results/precision-recall/{model_name}.png')

        plot_confusion_matrix(model, x_test, y_test)
        plt.savefig(f'../../results/confusion_matrix/{model_name}.png')


def main():
    x_train, x_test, y_train, y_test = load_and_split_data()
    make_plots(x_train, x_test, y_train, y_test)


if __name__ == '__main__':
    main()
