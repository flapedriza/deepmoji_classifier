#!/bin/bash -l
#
#SBATCH -J precision-recall
#SBATCH -o precision-recall.out
#SBATCH -e precision-recall.err
#
#SBATCH --mail-user francesc.lapedriza@est.fib.upc.edu
#SBATCH --mail-type=ALL
#
#SBATCH --mem=1024M
#SBATCH -c 1
#SBATCH -p short

export PYTHONUNBUFFERED=TRUE

virtualenv --python=/usr/bin/python3 "$TMPDIR"/env

source "$TMPDIR"/env/bin/activate

pip install --upgrade pip

pip install -r ../../requirements_plots.txt

python precision-recall.py